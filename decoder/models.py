from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
# Create your models here.


class DecodedAPRSFrames(models.Model):
    raw_frame = models.CharField(max_length=500)  # storing raw_frames
    author = models.CharField(max_length=9)  # storing the author of the frame
    recipient = models.CharField(max_length=9)  # storing the recipient of the frame
    path = models.CharField(max_length=200)  # storing paths the frames go through
    via = models.CharField(max_length=200)  # storing all the bases the frame goes through
    messagecapable = models.BooleanField(default=False)  # whether the frame has a message or no
    format = models.CharField(max_length=12)  # the format of the frame
    posambiguity = models.IntegerField(default=0)
    symbol = models.CharField(max_length=10)  # the symbol of the frame
    symbol_table = models.CharField(max_length=10)  # the symbol of the frame referenced by the table
    latitude = models.FloatField(default=0)  # latitude in the message
    longitute = models.FloatField(default=0)  # longitude in the message
    comment = models.CharField(max_length=100)  # comment in the message

    def __str__(self):
        return self.raw_frame  # for displaying the raw_frame in admin site

    class Meta:
        app_label = 'decoder'


class UndecodedAPRSFrames(models.Model):
    raw_frame = models.CharField(max_length=500)  # raw_frame of the frame
    error_msg = models.CharField(max_length=200)  # error message aprslib threw when decoding

    def __str__(self):
        return self.raw_frame  # for displaying the raw_frame in admin site

    class Meta:
        app_label = 'decoder'


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):  # Create the token after creating the user
    if created:  # This will make a token for created user automatically
        Token.objects.create(user=instance)