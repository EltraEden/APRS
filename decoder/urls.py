from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from . import views


router = DefaultRouter()
router.register(r'Decoded', views.DecodedAPRSFramesList)
router.register(r'Undecoded', views.UndecodedAPRSFramesList)

urlpatterns = [
    url(r'^', include(router.urls))
]

