from rest_framework import serializers
from .models import DecodedAPRSFrames, UndecodedAPRSFrames


class DecodedFramesSerializer(serializers.ModelSerializer):
    class Meta:
        model = DecodedAPRSFrames
        fields = '__all__'


class UndecodedFramesSerializer(serializers.ModelSerializer):
    class Meta:
        model = UndecodedAPRSFrames
        fields = '__all__'