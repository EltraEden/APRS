from django.test import TestCase
from rest_framework.test import APIClient  # I used APIClient() for this
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
import hashlib
# Create your tests here.


class AuthSystem(TestCase):  # Testing the authentication system

    def setUp(self):
        self.user = User.objects.create_user(username="test_user", password=hashlib.sha3_512("123".encode("utf-8")).hexdigest())  # Creating test user with hashed password
        self.client = APIClient()  # APIClient
        self.token = Token.objects.get(user=self.user)  # Get the token of test user

    def test_not_authenticated(self):  # Testing if you can access the data without logging in
        request = self.client.get('/aprs/Decoded/')  # This should return 401: Access Denied
        response = request.status_code
        self.assertEqual(response, 401)

    def test_authenticated(self):  # Accessing the data after login
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)  # Adding token to the header
        request = self.client.get('/aprs/Decoded/')
        response = request.status_code  # This should return 200: OK
        self.assertEqual(response, 200)

    def test_token_after_login(self):  # Testing the token received upon login
        data = {'username': 'test_user', 'password': '48c8947f69c054a5caa934674ce8881d02bb18fb59d5a63eeaddff735b0e9801e87294783281ae49fc8287a0fd86779b27d7972d3e84f0fa0d826d7cb67dfefc'}  # Data used for login
        request = self.client.post('/token/', data, format='json')  # Accessing the token from the django REST website
        response = request.json()['token']  # Getting the token from response
        self.assertEqual(response, self.token.key)   # The keys should be the same