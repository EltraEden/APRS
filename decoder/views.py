from rest_framework import viewsets
from decoder.models import DecodedAPRSFrames, UndecodedAPRSFrames
from decoder.serializers import DecodedFramesSerializer, UndecodedFramesSerializer
from rest_framework import permissions
from rest_framework.authentication import TokenAuthentication


class DecodedAPRSFramesList(viewsets.ReadOnlyModelViewSet):  # Data is read only
    queryset = DecodedAPRSFrames.objects.all()
    serializer_class = DecodedFramesSerializer
    authentication_classes = (TokenAuthentication,)  # Authentication based on Tokens
    permission_classes = (permissions.IsAuthenticated,)  # Restrict access for users that are not logged in


class UndecodedAPRSFramesList(viewsets.ReadOnlyModelViewSet):  # Same as above
    queryset = UndecodedAPRSFrames.objects.all()
    serializer_class = UndecodedFramesSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
